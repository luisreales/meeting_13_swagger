const swaggerUI = require('swagger-ui-express');
const swaggerJSDoc = require('swagger-jsdoc');

const express = require('express');
const server = express();

server.use(express.json()) // for parsing application/json
server.use(express.urlencoded({ extended: true })) // for parsing application/x-   www-form-urlencoded

/*
 * 
*Marca (char)
Modelo (char)
Fecha de fabricación (date)
Cantidad de puertas (entero)
Disponible para la venta (booleano)
*/

//1.instalamos swaggerUI

//npm i swagger-ui-express swagger-jsdoc --save

//2.importamos las librerias al proyecto

//3.definicion de opciones

const swaggerOptions = {
    swaggerDefinition:{
      info: {
        title: "Simple Todos API", // short title.
        description: "A simple todos API", //  desc.
        version: "1.0.0", // version number
        contact: {
          name: "Luis", // your name
          email: "luis@prueba.com", // your email
          url: "acamica.com", // your website
        },
      },
    },
    apis:['./server.js'],
};

//4.configuracion de SwaggerDocs
const swaggerDocs = swaggerJSDoc(swaggerOptions);

//5.configuracion de endpoint de swagger

server.use('/api-docs', swaggerUI.serve,swaggerUI.setup(swaggerDocs));

//6 configuracion de endpoint con swagger

let vechiculos = [
    {
        marca:"Ford",
        modelo:"Ford EcoSport.",
        fecha_fabricacion: new Date(25/10/2000),
        puertas: 4,
        disponible:true
    },
    {
        marca:"Mazda",
        modelo:"Mazda 3.",
        fecha_fabricacion: new Date(2/05/2015),
        puertas: 5,
        disponible:false
    }
];
  
/**
 * @swagger
 * /vechiculos:
 *  get:
 *    description: Lista nuevos vehiculos 
 *    responses:
 *      200:
 *        description: Success
 * 
 */
server.get('/vechiculos', function(req, res){
    return res.send(vechiculos);
});


/**
 * @swagger
 * /vechiculos:
 *  post:
 *    description: Agregar vehiculos
 *    parameters:
 *    - name: marca
 *      description: Marca carro
 *      in: formData
 *      required: true
 *      type: string
 *    - name: modelo
 *      description: model del carro
 *      in: formData
 *      required: true
 *      type: string
 *    - name: fecha
 *      description: fecha fabricacion
 *      in: formData
 *      required: true
 *      type: string
 *    - name: puertas
 *      description: numero de puertas
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: disponible
 *      description: disponibilidad
 *      in: formData
 *      required: true
 *      type: boolean
 *    responses:
 *      200:
 *        description: Success
 * 
 */
 server.post('/vechiculos', function(req, res){
  const { marca,modelo,fecha,puertas,disponible} = req.body;
  const vechiculo = {
    marca:marca,
    modelo:modelo,
    fecha_fabricacion:fecha,
    puertas:puertas,
    disponible: disponible
  }  
  vechiculos.push(vechiculo);
  return res.send(vechiculos);
});


/**
 * @swagger
 * /vechiculos:
 *  delete:
 *    description: borrar por nombre marca
 *    parameters:
 *    - name: marca
 *      in: formData
 *      description: nombre marca      
 *      required: true
 *      type: string 
 *    responses:
 *      200:
 *        description: Success
 * 
 */
 server.delete('/vechiculos', function(req, res){  
  console.log(req.body);
  if (!req.body.marca) {
    respuesta = {
      error: true,
      codigo: 502,
      mensaje: "El campo nombre es requerido",
    };
  } else {
    if (req.body.marca !== "") {
      let marca_data = req.body.marca;
      let filter_data = vechiculos.filter((x) => x.marca !== marca_data);
      vechiculos = [...filter_data];      
    }    
  }
  res.send(vechiculos);

});





/**
 * @swagger
 * /vechiculos:
 *  put:
 *    description: actualizar un vehiculo por marca
 *    parameters:
 *    - name: marca
 *      description: Marca carro
 *      in: formData
 *      required: true
 *      type: string
 *    - name: modelo
 *      description: model del carro
 *      in: formData
 *      required: true
 *      type: string
 *    - name: fecha
 *      description: fecha fabricacion
 *      in: formData
 *      required: true
 *      type: string
 *    - name: puertas
 *      description: numero de puertas
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: disponible
 *      description: disponibilidad
 *      in: formData
 *      required: true
 *      type: boolean
 *    responses:
 *      200:
 *        description: Success
 * 
 */
 server.put('/vechiculos', function(req, res){  
  console.log(req.body);
  if (!req.body.marca) {
    respuesta = {
      error: true,
      codigo: 502,
      mensaje: "El campo marca es requerido",
    };
  } else {
    if (req.body.marca !== "") {
      const { marca,modelo,fecha,puertas,disponible} = req.body;        
      vechiculos.map((item) => {
        if(item.marca === marca){
          item.modelo = modelo;
          item.fecha_fabricacion = fecha;
          item.puertas = puertas;
          item.disponible = disponible;
        }
      });

      respuesta = {
        error: false,
        codigo: 200,
        mensaje: "Marca "+ marca+" de vehiculos actualizado"       
      };
      console.log(respuesta);
    }    
  }
  res.send(vechiculos);

});


server.listen(3001, function(){
    console.log('servidor ejecutandose en el puerto 3001');
});

