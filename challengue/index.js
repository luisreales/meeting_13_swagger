const express = require('express');
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');

const server = express();

const swaggerOptions =  {
    swaggerDefinition:{
        info: {
            title: 'Acamica API',
            version:'1.0.0'
        }
    },
    apis:['./*.js']
};


const swaggerDocs = swaggerJsDoc(swaggerOptions);

server.use('/api-docs', 
            swaggerUI.serve,
            swaggerUI.setup(swaggerDocs));

//documentando endpoints//


/**
 * @swagger
 * /estudiantes:
 *  post:
 *    description: Crea un nuevo estudiante
 *    parameters:
 *    - name: nombre
 *      description: Nombre del estudiante
 *      in: formData
 *      required: true
 *      type: string
  *    - name: edad
 *      description: Edad del estudiante
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 * 
 */
 server.post('/estudiantes', (req, res) => {
    res.status(201).send();
  });


  /**
 * @swagger
 * /estudiantes:
 *  get:
 *      description: Obtener listado estudiantes     
 *  responses:
 *      200:
 *        description: Success
 */
server.get('/estudiantes', (req, res) => {
    res.status(201).send('listado');
  });

  /**
 * @swagger
 * /estudiantes:
 *  put:
 *
 */
server.put('/estudiantes', (req, res) => {
    res.status(201).send();
  });

    /**
 * @swagger
 * /estudiantes:
 *  patch:
 *
 */
server.patch('/estudiantes', (req, res) => {
    res.status(201).send();
  });

      /**
 * @swagger
 * /estudiantes:
 *  delete:
 *
 */
server.patch('/estudiantes', (req, res) => {
    res.status(201).send();
  });
  
  
  

server.listen(5000,function(){
    console.log("ejecutandose por servidor 5000 http://localhost:5000");
}); 
